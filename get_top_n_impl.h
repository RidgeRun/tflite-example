//Copyright 2017 The TensorFlow Authors. All Rights Reserved.

#ifndef TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_IMPL_H_
#define TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_IMPL_H_

#include <algorithm>
#include <functional>
#include <queue>

namespace tflite {
namespace example {

extern bool input_floating;

// Returns the top N confidence values over threshold in the provided vector,
// sorted by confidence in descending order.
template <class T>
static void get_top_n(T* prediction, int prediction_size, size_t num_results,
               float threshold, std::vector<std::pair<float, int>>* top_results,
               bool input_floating) {
  // Will contain top N results in ascending order.
  std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>,
                      std::greater<std::pair<float, int>>>
      top_result_pq;

  const long count = prediction_size;  // NOLINT(runtime/int)
  for (int i = 0; i < count; ++i) {
    float value;
    if (input_floating)
      value = prediction[i];
    else
      value = prediction[i] / 255.0;
    // Only add it if it beats the threshold and has a chance at being in
    // the top N.
    if (value < threshold) {
      continue;
    }

    top_result_pq.push(std::pair<float, int>(value, i));

    // If at capacity, kick the smallest value out.
    if (top_result_pq.size() > num_results) {
      top_result_pq.pop();
    }
  }

  // Copy to output vector and reverse into descending order.
  while (!top_result_pq.empty()) {
    top_results->push_back(top_result_pq.top());
    top_result_pq.pop();
  }
  std::reverse(top_results->begin(), top_results->end());
}

}  // namespace example
}  // namespace tflite

#endif  // TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_IMPL_H_
