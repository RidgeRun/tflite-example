//Copyright 2017 The TensorFlow Authors. All Rights Reserved.

#ifndef TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_H_
#define TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_H_

#include "get_top_n_impl.h"

namespace tflite {
namespace example {

template <class T>
void get_top_n(T* prediction, int prediction_size, size_t num_results,
               float threshold, std::vector<std::pair<float, int>>* top_results,
               bool input_floating);

// explicit instantiation so that we can use them otherwhere
template void get_top_n<uint8_t>(uint8_t*, int, size_t, float,
                                 std::vector<std::pair<float, int>>*, bool);
template void get_top_n<float>(float*, int, size_t, float,
                               std::vector<std::pair<float, int>>*, bool);

}  // namespace example
}  // namespace tflite

#endif  // TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_GET_TOP_N_H_
