TensorflowPath = pathtotensorflow
CXX=g++
CXXFLAGS=-std=c++11 \
	 -I $(TensorflowPath)/ \
	 -I $(TensorflowPath)/tensorflow/lite/tools/make/downloads/flatbuffers/include \
	 -l tensorflow-lite \
	 -pthread

SRC=tflite_example.cc
BIN=tflite_example

.PHONY: clean all

all: $(BIN)

$(BIN): $(SRC)
	$(CXX) -o $(BIN) $(SRC) $(CXXFLAGS)

clean: 
	@rm -f $(BIN)

