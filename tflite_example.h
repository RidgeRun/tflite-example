//Copyright 2017 The TensorFlow Authors. All Rights Reserved.

#ifndef TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_TFLITE_EXAMPLE_H_
#define TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_TFLITE_EXAMPLE_H_

#include "tensorflow/lite/model.h"
#include "tensorflow/lite/string_type.h"

namespace tflite {
namespace example {

struct Settings {
  bool verbose = false;
  bool accel = false;
  bool old_accel = false;
  bool input_floating = false;
  bool profiling = false;
  bool allow_fp16 = false;
  bool gl_backend = false;
  int loop_count = 1;
  float input_mean = 127.5f;
  float input_std = 127.5f;
  string model_name = "./models/mobilenet_v1_1.0_224_quant.tflite";
  tflite::FlatBufferModel* model;
  string input_file_name = "./testdata/dog.jpg";
  string labels_file_name = "./models/labels.txt";
  string input_layer_type = "uint8_t";
  int number_of_threads = 4;
  int number_of_results = 5;
  int max_profiling_buffer_entries = 1024;
  int number_of_warmup_runs = 2;
};

}  // namespace example
}  // namespace tflite

#endif  // TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_TFLITE_EXAMPLE_H_
