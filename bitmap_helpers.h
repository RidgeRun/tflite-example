//Copyright 2017 The TensorFlow Authors. All Rights Reserved.

#ifndef TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_BITMAP_HELPERS_H_
#define TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_BITMAP_HELPERS_H_

#include "bitmap_helpers_impl.h"
#include "tflite_example.h"

namespace tflite {
namespace example {

static std::vector<uint8_t> read_bmp(const std::string& input_file_name, int* width,
                              int* height, int* channels, Settings* s);

template <class T>
static void resize(T* out, uint8_t* in, int image_height, int image_width,
            int image_channels, int wanted_height, int wanted_width,
            int wanted_channels, Settings* s);

// explicit instantiation
template void resize<uint8_t>(uint8_t*, unsigned char*, int, int, int, int, int,
                              int, Settings*);
template void resize<float>(float*, unsigned char*, int, int, int, int, int,
                            int, Settings*);

}  // namespace example
}  // namespace tflite

#endif  // TENSORFLOW_LITE_EXAMPLES_TFLITE_EXAMPLE_BITMAP_HELPERS_H_
