
# Tensorflow Lite Example

## __Description:__

This example shows how to use the tensorflow-lite api using C++.

## __Dependencies:__

Please follow the next tutorial to build, install and run this example with tflite:
https://developer.ridgerun.com/wiki/index.php?title=Tensorflow_Lite_Example


## __Usage:__

* Change in the Makefile the pathtotensorflow to the tensorflow path in the system

* Compile the source code.
```
make
```

* Execute the generated progam.
```
./tflite_example --tflite_model models/mobilenet_v1_1.0_224.tflite --labels models/labels.txt --image testdata/dog.jpg -v 1

```
